import React, { Component } from 'react';
import './App.css';
import Touche from './Touche'

const MOTS = ["JUPITER","CELEBRITE","COW-BOY","MIROIR","BALCON","VAPEUR","TOTEM","SKATEBOARD","AMITIE","GYMNASTIQUE","EQUIPE","TROTTOIR","ECHAPPEMENT","ELEPHANT","HIMALAYA","VERDICT","BATON","TERRASSEMENT","RUSSIE"];
const LETTRES = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const ESSAIS = 11;

class App extends Component {
    state = {
        mot : "",
        longueur : 0,
        lettres: [],
        essais: ESSAIS,
        lettresEssayees: []
    }

    initState() {
        let mot = this.piocheMot(), long = this.longueurDuMot(mot);
        console.log("Le mot à trouver est "+mot);
        this.setState({
            mot:mot,
            longueur:long,
            lettres: [],
            essais : ESSAIS,
            lettresEssayees : []
        });
    }

    longueurDuMot(str) {
        var nb = 0;
        if(typeof str !== "undefined") {
            for (var i = 0; i < str.length; i++) {
                if(str.charAt(i).match(/\w/)) {
                    nb++;
                }
            }
        }
        return nb;
    }

    computeDisplay(phrase, usedLetters) {
        return phrase.replace(/\w/g,
            (letter) => (usedLetters.includes(letter.toUpperCase()) ? letter : '_')
        )
    }

    piocheMot() {
        var i = Math.floor(Math.random() * (MOTS.length - 1));
        return MOTS[i];
    }

    /*Binding du this*/
    essayeLettre = (x)=> {
        const {mot,lettresEssayees,essais,longueur} = this.state;
        if(essais===0 || longueur === 0 || lettresEssayees.includes(x.toUpperCase())) return;
        const newLettresEssayees = [...lettresEssayees,...x];
        var reg = new RegExp(x.toUpperCase(),"g");
        var nb = (mot.match(reg)||[]).length;
        if(nb===0) {
            console.log(x+" : Perdu");
            this.setState({essais:essais-1})
        }
        else {
            console.log(x+" : Trouvé "+nb+" fois! Reste à trouver "+(longueur-nb)+" lettres");
            this.setState({longueur:longueur-nb})
        }
        this.setState({lettresEssayees:newLettresEssayees})

    }

    feedbackForLetter(x) {
        const {lettresEssayees} = this.state;
        return lettresEssayees.includes(x) ?  "Grisee" : "";
    }

    componentWillMount() {
        this.initState();
    }
    render() {
        const {mot,essais,lettresEssayees,longueur} = this.state;
        const clavier = LETTRES.split("");
        const motCache = this.computeDisplay(mot,lettresEssayees);
        var essais_str = essais.toString()+" essai";
        var resultat,result;
        if(essais>1) {
            essais_str += "s";
        }
        if(longueur===0) {
            resultat = "Vous avez gagné ! Bravo !";
            result = "Won";
        }
        else if(essais===0) {
            resultat = "Vous avez perdu ! Le mot était : "+mot+". Essayez encore !";
            result = "Lost";
        }
        return (
            <div className="App">
                <header className="App-header">
                <h1 className="App-title">Devinez le mot</h1>
                </header>
                <div className="Mot">{motCache}</div>
                <div className="Compteur">Il vous reste {essais_str}</div>
                <div className={`Resultat ${result}`}>{resultat}</div>
                {result ?
                    (<div className="Reinit" onClick={()=>{this.initState()}}>Une autre partie ?</div>)
                    : (<div className="Clavier">
                        {clavier.map((x, index) => (
                            <Touche
                                lettre={x}
                                index={index}
                                key={index}
                                feedback={this.feedbackForLetter(x)}
                                onClick={this.essayeLettre}
                            />
                        ))}
                    </div>)
                }
            </div>
        );
    }
}

export default App;
