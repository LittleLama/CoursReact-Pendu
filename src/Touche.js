import React from 'react'

import './Touche.css'

const Touche = ({lettre, feedback, onClick }) => (
    <div className={`Touche ${lettre} ${feedback}` } onClick={()=>onClick(lettre)}>
        {lettre}
    </div>
)

export default Touche